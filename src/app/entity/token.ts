export class Token {
  private _token_type: string;
  private _expires_in: string;
  private _access_token: string;
  private _refresh_token: string;

  constructor(token_type: string, expires_in: string, access_token: string, refresh_token: string) {
    this._token_type = token_type;
    this._expires_in = expires_in;
    this._access_token = access_token;
    this._refresh_token = refresh_token;
  }

  get token_type(): string {
    return this._token_type;
  }

  set token_type(value: string) {
    this._token_type = value;
  }

  get expires_in(): string {
    return this._expires_in;
  }

  set expires_in(value: string) {
    this._expires_in = value;
  }

  get access_token(): string {
    return this._access_token;
  }

  set access_token(value: string) {
    this._access_token = value;
  }

  get refresh_token(): string {
    return this._refresh_token;
  }

  set refresh_token(value: string) {
    this._refresh_token = value;
  }
}
