import { Component } from '@angular/core';
import {AuthService} from "./service/auth.service";
import {FormBuilder, FormGroup} from "@angular/forms";
import {Router} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  options: FormGroup;
  viewSidenavBig = true;

  constructor(private fb: FormBuilder, public auth: AuthService, private router: Router) {
    this.options = fb.group({
      bottom: 0,
      fixed: false,
      top: 0
    });
  }

  changeSidenav() {
    this.viewSidenavBig = !this.viewSidenavBig;
  }

  logout() {
    this.auth.logout();
    this.router.navigate(['/login']);
  }
}
