import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import {AuthService} from "../../service/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent {
  addressForm = this.fb.group({
    email: [null, Validators.required],
    password: [null, Validators.required],
  });

  constructor(private fb: FormBuilder, private auth: AuthService, private router: Router) {
    if(this.auth.isLoggedIn()) {
      this.router.navigate(['/']);
    }
  }

  onSubmit(): void {
    this.auth.login(this.addressForm.get('email')?.value, this.addressForm.get('password')?.value)
      .subscribe(
        result => this.router.navigate(['/'])
      );
  }
}
