import { Injectable } from '@angular/core';
import { environment } from './../../environments/environment';
import {HttpClient, HttpErrorResponse, HttpHeaders} from "@angular/common/http";
import { Observable, throwError } from 'rxjs';
import {retry, catchError, tap} from 'rxjs/operators';
import {Token} from "../entity/token";


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private _token!: string | null;
  public key: string = 'currentUser';
  private _isLogged!: boolean;

  constructor(private httpClient: HttpClient) {
    if (localStorage.getItem(this.key) != null) {
      let currentUser = JSON.parse(<string>localStorage.getItem(this.key));
      this.token = currentUser && currentUser.token;
      this.isLogged = this.isLoggedIn();
    }
  }

  login(email: string, password: string): Observable<Token> {
    let body = {username: email, password: password};

    return this.httpClient.post<Token>(environment.apiUrl+'api/auth/login/', body)
      .pipe(
        tap(
          data => {
            this.isLogged = true;
            this.token = data.access_token;
            localStorage.setItem(this.key, JSON.stringify({email: email, token: data.access_token}));
          },
          error => {
            this.isLogged = false;
          }
        ),
        catchError(AuthService.handleError)
      )
  }

  private static handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, body was: `, error.error);
    }
    // Return an observable with a user-facing error message.
    return throwError('Something bad happened; please try again later.');
  }

  logout(): void {
    // clear token remove user from local storage to log user out
    this._token = null;
    this._isLogged = false;
    localStorage.removeItem(this.key);
  }

  isLoggedIn(): boolean {
    let item = localStorage.getItem(this.key);
    return item != null;
  }

  get isLogged(): boolean {
    return this._isLogged;
  }

  set isLogged(value: boolean) {
    this._isLogged = value;
  }
  get token(): string | null {
    return this._token;
  }

  set token(value: string | null) {
    this._token = value;
  }
}
